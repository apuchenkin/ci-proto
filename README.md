# CI Proto #

The prototype uses Vagrant environment manager. Currently only KVM and virtualbox (Linux/Windows) are supported.

To set KVM virtualization up on Ubuntu use the following:
```
# make sure your pcu supports virtualization
$ cat /proc/cpuinfo | egrep '(vmx|svm)'

# make sure you have x64 architecture and turn on hardware virtualization in BIOS
$ uname -a

# install KVM
$ sudo apt-get install qemu-kvm libvirt-bin ubuntu-vm-builder bridge-utils
$ sudo reboot

# verify that KVM installed successfully
$ virsh -c qemu:///system list

# install vagrant
$ wget https://releases.hashicorp.com/vagrant/1.8.6/vagrant_1.8.6_x86_64.deb
$ dpkg -i vagrant_1.8.6_x86_64.deb

# install dependencies for vagrant libvirt plugin
$ sudo apt-get install libxslt-dev libxml2-dev libvirt-dev zlib1g-dev ruby-dev

# install vagrant libvirt plugin
$ vagrant plugin install vagrant-libvirt --verbose

# install vagrant cachier plugin (optional)
$ vagrant plugin install vagrant-cachier --verbose
```

Use the following commands to manage the environment:
```
# start the box using virtualbox virtualization
$ vagrant up ci --provider=virtualbox

# start the box using kvm virtualization
$ vagrant up ci --provider=libvirt

# turn off the box
$ vagrant halt ci

# remove the box (all data will be vanished)
$ vagrant destroy ci

# reload the box with provision only
$ vagrant reload ci --provision
```

This will setup new Ubuntu virtual box, then provision the box with ansible, java, maven, docker, jenkins, jenkins plugins and add sample jenkins jobs. Sample jenkins jobs package the java project producing WAR and deploy WAR to the docker container.


**Jenkins can be accessed at 127.0.0.1:8080**

*Access: user/user*

**Tomcat application can be accessed at 127.0.0.1:8888, after successful Jenkins pipeline run**